let currentLanguage = 'English';
const language = {
  English: {
    navigation: '<i class="las la-arrow-left"></i> Back to Mile',
    caption: 'Your one stop platform to manage all of your field service management',
    organizationLabel: 'Organization Name',
    login: 'Login',
    register: 'Not registered yet? <a href="#">Contact us</a> for more info',
    emptyOrganization: 'The organization field is required',
    contact: 'Call Us: +62 812-1133-5608',
    notExist: 'is not available',
    alternativeLanguage: 'Indonesia'
  },
  Indonesia: {
    navigation: '<i class="las la-arrow-left"></i> Kembali ke Mile',
    caption: 'Platform Anda untuk mengelola semua manajemen layanan di lapangan',
    organizationLabel: 'Nama Organisasi',
    login: 'Masuk',
    register: 'Belum terdaftar? <a href="#">Hubungi kami</a> untuk info lebih lanjut',
    emptyOrganization: 'Nama organisasi dibutuhkan',
    contact: 'Hubungi Kami: +62 812-1133-5608',
    notExist: 'tidak tersedia',
    alternativeLanguage: 'English'
  }
}
const login = (e) => {
  const input = document.getElementById('organization');
  const value = input.value;
  if(!value) {
    showWarning(input, language[currentLanguage].emptyOrganization);
    return
  };
  e.target.toggleAttribute('disabled');
  hideWarning(input);
  // pretend hit API
  e.target.innerHTML = '<i class="las la-spinner la-spin la-lg"></i>';
  setTimeout(() => {
    e.target.toggleAttribute('disabled');

    showWarning(input, `oops :( ${value}.mile.app ${language[currentLanguage].notExist}`);
    e.target.innerHTML = language[currentLanguage].login;
  }, 500)
}

const checkUserInput = (e) => {
  const input = document.getElementById('organization');
  const value = input.value;
  if (!value && e.keyCode === 8) {
    showWarning(input, language[currentLanguage].emptyOrganization);
    return false;
  }
  hideWarning(input);
  return true;
}

const showWarning = (input, message) => {
  if (input.parentElement.className.includes('error')) return;
  input.parentElement.className = 'form-data organization error';
  input.parentElement.insertAdjacentHTML("beforeend", `<span id="error-message" class="validate-failed">${message}</span>`);
}
const hideWarning = (input) => {
  input.parentElement.className = 'form-data organization';
  const warning = input.parentElement.querySelector('span');
  if (!warning) return;
  warning.remove();
}

const checkLanguage = () => {
  currentLanguage = localStorage.language || 'English';
  localStorage.setItem('language', currentLanguage);
  document.getElementById('alternativeLanguage').innerText = language[currentLanguage].alternativeLanguage
}

const toggleLanguageList = () => {
  const alternative = document.getElementById('alternativeLanguage');
  const arrow = document.getElementById('arrow');
  if (alternative.className.includes('hidden')) {
    alternative.className = 'alternative-language';
    arrow.style.transform = 'rotate(180deg)';
  } else {
    alternative.className = 'alternative-language hidden';
    arrow.style.transform = 'rotate(0deg)';
  }
}

const adjustLanguage = () => {
  const elements = ['navigation', 'caption', 'organizationLabel', 'validate-failed', 'login', 'register', 'contact', 'alternativeLanguage'];
  elements.forEach((item) => {
    const element = document.getElementById(item);
    if (element) {
      element.innerHTML = language[currentLanguage][item];
    }
  })
  document.getElementById('currentLanguage').innerHTML = `${currentLanguage} <i id="arrow" class="las la-angle-down"></i>`;
  hideWarning(document.getElementById('organization'));
}

const changeLanguage = () => {
  const alternative = document.getElementById('alternativeLanguage');
  alternative.className = 'alternative-language hidden';
  currentLanguage = alternative.innerText;
  localStorage.setItem('language', currentLanguage);
  adjustLanguage();
}

const hideLanguageListIfClickBesideToggle = () => {
  const bodyClick = (event) => {
    if(event.target != currentLanguageElement) {
      document.getElementById('alternativeLanguage').className = 'alternative-language hidden';
      document.getElementById('arrow').style.transform = 'rotate(0deg)';
    }
  }
  const body = document.getElementById('body')
  const currentLanguageElement = document.getElementById('currentLanguage');
  
  body.addEventListener("click", bodyClick, false);
  
}

checkLanguage();
adjustLanguage();
hideLanguageListIfClickBesideToggle();