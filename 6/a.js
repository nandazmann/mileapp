// Swap the values of variables A and B
A = 3;
B = 5;

A = A + B;
B = A - B;
A = A - B;

console.log(A, B);