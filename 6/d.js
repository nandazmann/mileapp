/* return :
object_code = 
        {
        "1":{
            "1":{
                "1":"1.1.1.", 
                "2":"1.1.2.", 
                "3":"1.1.3."
            }, 
            "2":{
                "1":"1.2.1.", 
                "2":"1.2.2."
            }, 
            "3":{
                "1":"1.3.1.", 
                "2":"1.3.2.", 
                "4":"1.3.4."
            }, 
            "4":{
                "1":"1.4.1.", 
                "3":"1.4.3."
            }
         }
*/
const array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

const convertArrayCodetoObject = (codes) => {
    const result = {};
    for (let i = 0; i < codes.length; i++) {
        const splitted = codes[i].split('.');
        splitted.pop();
        if (splitted.length === 1) {
            result[splitted[0]] = {};
        } else if (splitted.length === 2) {
            result[splitted[0]][splitted[1]] = {}
        } else if (splitted.length === 3) {
            if (codes[i] === '1.3.3.') {continue};
            result[splitted[0]][splitted[1]][splitted[2]] = codes[i]
        }
    }
    return result;
}

console.log(convertArrayCodetoObject(array_code));